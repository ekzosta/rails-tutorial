class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@ekzoku.herokuapp.com'
  layout 'mailer'
end
